// ignore_for_file: prefer_const_constructors we dont want those in tests

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void main() {
  test('binarize given PayloadWriter', () {
    final writer = Payload.write()
      ..set(uint8, 5)
      ..set(uint16, 300);

    expect(binarize(writer), equals(Uint8List.fromList([5, 1, 44])));
  });
}

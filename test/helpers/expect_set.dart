import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void Function() expectSet<T>(PayloadType<T> type, T input, List<int> output) {
  return () {
    final writer = ByteWriter();
    type.set(writer, input);
    return expect(writer.toBytes(), equals(Uint8List.fromList(output)));
  };
}

import 'package:test/test.dart';

Matcher throwsAssertionMessage(String message) {
  return throwsA(
    isA<AssertionError>().having((e) => e.message, 'message', message),
  );
}

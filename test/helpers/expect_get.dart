import 'dart:typed_data';

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void Function() expectGet<T>(PayloadType<T> type, List<int> input, T output) {
  return () {
    final result = type.get(ByteReader(Uint8List.fromList(input)));
    if (output is Float32x4) {
      if (result is Float32x4) {
        expect(result.x, output.x);
        expect(result.y, output.y);
        expect(result.z, output.z);
        expect(result.w, output.w);
      }
      return expect(result, isA<Float32x4>());
    }

    if (output is Int32x4) {
      if (result is Int32x4) {
        expect(result.x, output.x);
        expect(result.y, output.y);
        expect(result.z, output.z);
        expect(result.w, output.w);
      }
      return expect(result, isA<Int32x4>());
    }

    if (output is Float64x2) {
      if (result is Float64x2) {
        expect(result.x, output.x);
        expect(result.y, output.y);
      }
      return expect(result, isA<Float64x2>());
    }

    if (output is TypedData) {
      if (result is TypedData) {
        expect(
          result.buffer.asUint8List(),
          equals(output.buffer.asUint8List()),
        );
      }
      return expect(result, isA<T>());
    }

    return expect(result, equals(output));
  };
}

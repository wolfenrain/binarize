// ignore_for_file: prefer_const_constructors we dont want those in tests
// ignore_for_file: deprecated_member_use_from_same_package

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

import '../helpers/helpers.dart';

void main() {
  group('nullable', () {
    test('returns null', expectGet(nullable(uint8), [0], null));

    test('stores null', expectSet(nullable(uint8), null, [0]));
  });

  group('string8', () {
    const data = 'Hello World';
    final bytes = [
      11, 72, 101, 108, 108, //
      111, 32, 87, 111,
      114, 108, 100,
    ];

    test('returns "$data"', expectGet(string8, bytes, data));

    test('sets bytes to "$data"', expectSet(string8, data, bytes));
  });

  group('string16', () {
    const data = 'Hello World';
    final bytes = [
      0, 11, 72, //
      101, 108, 108,
      111, 32, 87, 111,
      114, 108, 100,
    ];

    test('returns "$data"', expectGet(string16, bytes, data));

    test('sets bytes to "$data"', expectSet(string16, data, bytes));
  });

  group('string32', () {
    const data = 'Hello World';
    final bytes = [
      0, 0, 0, 11, //
      72, 101, 108, 108,
      111, 32, 87, 111,
      114, 108, 100,
    ];

    test('returns "$data"', expectGet(string32, bytes, data));

    test('sets bytes to "$data"', expectSet(string32, data, bytes));
  });

  group('string64', () {
    const data = 'Hello World';
    final bytes = [
      0, 0, 0, 0, //
      0, 0, 0, 11,
      72, 101, 108, 108,
      111, 32, 87, 111,
      114, 108, 100,
    ];

    test('returns "$data"', expectGet(string64, bytes, data));

    test('sets bytes to "$data"', expectSet(string64, data, bytes));
  });
}

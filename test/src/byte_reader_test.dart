import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void main() {
  group('$ByteReader', () {
    test('exposes the byte buffer that it uses', () {
      final reader = ByteReader(Uint8List.fromList([1, 2, 3, 4]));
      expect(reader.buffer, isA<ByteBuffer>());
    });

    test('get total length of the data', () {
      final reader = ByteReader(Uint8List.fromList([1, 2, 3, 4]));
      expect(reader.length, equals(4));
    });

    test('get length of the read data', () {
      final reader = ByteReader(Uint8List.fromList([1, 2, 3, 4]))..uint8();

      expect(reader.offset, equals(1));
      expect(reader.remainingBytes, equals(3));
    });

    test('returns true when done reading', () {
      final reader = ByteReader(Uint8List.fromList([1, 2, 3, 4]))..uint32();

      expect(reader.isDone, equals(true));
      expect(reader.isNotDone, equals(false));
    });

    test('can skip reading data', () {
      final reader = ByteReader(Uint8List.fromList([1, 2, 3, 4]));
      expect(reader.offset, equals(0));
      reader.skip(2);
      expect(reader.offset, equals(2));
    });

    test('can reset the reader', () {
      final reader = ByteReader(Uint8List.fromList([1, 2, 3, 4]))..uint32();
      expect(reader.remainingBytes, equals(0));
      reader.reset();
      expect(reader.length, equals(4));
    });
  });
}

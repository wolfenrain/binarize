import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void main() {
  group('$ByteWriter', () {
    test('get length of written data', () {
      final writer = ByteWriter()..uint8(10);
      expect(writer.length, equals(1));
      writer.uint16(16);
      expect(writer.length, equals(3));
    });

    test('can clear the written data', () {
      final writer = ByteWriter()..uint8(10);
      expect(writer.length, equals(1));
      writer.clear();
      expect(writer.length, equals(0));
    });
  });
}

// ignore_for_file: prefer_const_constructors we dont want those in tests
// ignore_for_file: avoid_equals_and_hash_code_on_mutable_classes

import 'dart:typed_data';

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

import '../helpers/helpers.dart';

class Person {
  const Person({
    required this.firstName,
    required this.lastName,
    required this.age,
    this.children = const [],
  });

  final String firstName;

  final String lastName;

  final int age;

  final List<Person> children;

  @override
  bool operator ==(Object other) {
    return other is Person &&
        other.firstName == firstName &&
        other.lastName == lastName &&
        other.age == age &&
        other.children.every(children.contains);
  }

  @override
  int get hashCode => Object.hashAll([firstName, lastName, age, children]);

  @override
  String toString() =>
      '''Person(firstName: $firstName, lastName: $lastName, age: $age, children: $children)''';
}

class PersonContract extends BinaryContract<Person> implements Person {
  const PersonContract()
      : super(const Person(firstName: '', lastName: '', age: 42));

  @override
  Person order(Person contract) {
    return Person(
      firstName: contract.firstName,
      lastName: contract.lastName,
      age: contract.age,
      children: contract.children,
    );
  }

  @override
  String get firstName => type(string.utf8(), (o) => o.firstName);

  @override
  String get lastName => type(string.utf8(), (o) => o.lastName);

  @override
  int get age => type(uint8, (o) => o.age);

  @override
  List<Person> get children =>
      type(list(personContract, lengthType: uint8), (o) => o.children);
}

final personContract = PersonContract();

enum TestEnum { a, b, c, d }

void main() {
  group('$BinaryContract', () {
    final output = [4, 74, 111, 104, 110, 3, 68, 111, 101, 42, 0];
    final person = Person(firstName: 'John', lastName: 'Doe', age: 42);

    test(
      'turns instance to binary using the contract',
      expectGet(personContract, output, person),
    );

    test(
      'turns binary to an instance using the contract',
      expectSet(personContract, person, output),
    );

    test('supports self-referencing types', () {
      final mom = Person(
        firstName: 'Jane',
        lastName: 'Doe',
        age: 89,
        children: [person],
      );
      final output = [
        4, 74, 97, 110, 101, 3, 68, 111, //
        101, 89, 1, 4, 74, 111, 104,
        110, 3, 68, 111, 101, 42, 0,
      ];

      expectGet(personContract, output, mom)();
      expectSet(personContract, mom, output)();
    });
  });

  group('boolean', () {
    test('returns true', expectGet(boolean, [1], true));
    test('returns false', expectGet(boolean, [0], false));

    test('sets output to true', expectSet(boolean, true, [1]));
    test('sets output to false', expectSet(boolean, false, [0]));
  });

  group('$Bytes', () {
    test('returns list of output', expectGet(Bytes(3), [1, 2, 3], [1, 2, 3]));

    test(
      'sets output to output',
      expectSet(Bytes(3), [1, 2, 3], [1, 2, 3]),
    );
  });

  group('enumeration', () {
    test(
      'returns list of output',
      expectGet(enumeration(TestEnum.values), [1], TestEnum.b),
    );

    test(
      'sets output to output',
      expectSet(enumeration(TestEnum.values), TestEnum.b, [1]),
    );
  });

  group('flags', () {
    final output = [5];
    final testFlags = [true, false, true, false, false, false, false, false];

    test(
      'returns correct flags from byte',
      expectGet(flags, output, testFlags),
    );

    test('sets byte to represent flags', expectSet(flags, testFlags, output));

    test('throws assertion when a list of more than 8 booleans is passed', () {
      expect(
        () => flags.set(ByteWriter(), [...testFlags, true]),
        throwsAssertionMessage('Flags can only be 8 bits long'),
      );
    });
  });

  group('float32', () {
    final output = [63, 192, 0, 0];

    test('returns 1.5', expectGet(float32, output, 1.5));

    test('sets output to 1.5', expectSet(float32, 1.5, output));
  });

  group('float32x4', () {
    final output = [
      191, 128, 0, 0, //
      0, 0, 0, 0,
      191, 128, 0, 0,
      0, 0, 0, 0,
    ];

    test(
      'returns Float32x4',
      expectGet(float32x4, output, Float32x4(-1, 0, -1, 0)),
    );

    test(
      'sets output to Float32x4',
      expectSet(float32x4, Float32x4(-1, 0, -1, 0), output),
    );
  });

  group('float64', () {
    final output = [63, 248, 0, 0, 0, 0, 0, 0];

    test('returns 1.5', expectGet(float64, output, 1.5));

    test('sets output to 1.5', expectSet(float64, 1.5, output));
  });

  group('float64x2', () {
    final output = [
      191, 240, 0, 0, 0, 0, 0, 0, //
      0, 0, 0, 0, 0, 0, 0, 0,
    ];

    test(
      'returns Float64x2',
      expectGet(float64x2, output, Float64x2(-1, 0)),
    );

    test(
      'sets output to Float64x2',
      expectSet(float64x2, Float64x2(-1, 0), output),
    );
  });

  group('int8', () {
    test('returns -8', expectGet(int8, [248], -8));
    test('returns 8', expectGet(int8, [8], 8));

    test('sets output to -8', expectSet(int8, -8, [248]));
    test('sets output to 8', expectSet(int8, 8, [8]));
  });

  group('int16', () {
    test('returns -16', expectGet(int16, [255, 240], -16));
    test('returns 16', expectGet(int16, [0, 16], 16));

    test('sets output to -16', expectSet(int16, -16, [255, 240]));
    test('sets output to 16', expectSet(int16, 16, [0, 16]));
  });

  group('int32', () {
    test('returns -32', expectGet(int32, [255, 255, 255, 224], -32));
    test('returns 32', expectGet(int32, [0, 0, 0, 32], 32));

    test('sets output to -32', expectSet(int32, -32, [255, 255, 255, 224]));
    test('sets output to 32', expectSet(int32, 32, [0, 0, 0, 32]));
  });

  group('int32x4', () {
    const output = [
      255, 255, 255, 255, //
      0, 0, 0, 0,
      255, 255, 255, 255,
      0, 0, 0, 0,
    ];
    test(
      'returns Int32x4',
      expectGet(int32x4, output, Int32x4(-1, 0, -1, 0)),
    );

    test(
      'sets output to Int32x4',
      expectSet(int32x4, Int32x4(-1, 0, -1, 0), output),
    );
  });

  group('int64', () {
    test(
      'returns -64',
      expectGet(int64, [255, 255, 255, 255, 255, 255, 255, 192], -64),
    );
    test('returns 64', expectGet(int64, [0, 0, 0, 0, 0, 0, 0, 64], 64));

    test(
      'sets output to -64',
      expectSet(int64, -64, [255, 255, 255, 255, 255, 255, 255, 192]),
    );
    test('sets output to 64', expectSet(int64, 64, [0, 0, 0, 0, 0, 0, 0, 64]));
  });

  group('list', () {
    final output = [0, 0, 0, 3, 0, 1, 0, 2, 0, 3];

    test(
      'returns a list',
      expectGet(list(uint16), output, [1, 2, 3]),
    );

    test('sets output to a list', expectSet(list(uint16), [1, 2, 3], output));
  });

  group('map', () {
    final input = {'1': 1, '2': 2, '3': 3, '4': 4};
    final output = [4, 1, 49, 0, 1, 1, 50, 0, 2, 1, 51, 0, 3, 1, 52, 0, 4];

    test('returns a map', expectGet(map(string.utf8(), uint16), output, input));

    test(
      'sets output to a map',
      expectSet(map(string.utf8(), uint16), input, output),
    );
  });

  group('nil', () {
    test('returns null', expectGet(nil(uint8), [0], null));

    test('stores null', expectSet(nil(uint8), null, [0]));

    test(
      'returns value if value is not null',
      expectGet(nil(uint8), [1, 2], 2),
    );

    test('sets value if value is not null', expectSet(nil(uint8), 2, [1, 2]));
  });

  group('$RawList', () {
    const input = ['1', '2', '3'];
    final output = [1, 49, 1, 50, 1, 51];

    test(
      'returns raw list of string',
      expectGet(RawList(string.utf8(), amount: 3), output, input),
    );

    test(
      'sets output to raw list of string',
      expectSet(RawList(string.utf8(), amount: 3), input, output),
    );
  });

  group('string', () {
    group('utf8', () {
      const input = 'Hello World';
      final output = [
        11, 72, 101, 108, 108, //
        111, 32, 87, 111,
        114, 108, 100,
      ];

      test('returns "$input"', expectGet(string.utf8(), output, input));

      test('sets output to "$input"', expectSet(string.utf8(), input, output));
    });

    group('latin1', () {
      const input = 'Hello World';
      final output = [
        11, 72, 101, 108, 108, //
        111, 32, 87, 111,
        114, 108, 100,
      ];

      test('returns "$input"', expectGet(string.latin1(), output, input));

      test(
        'sets output to "$input"',
        expectSet(string.latin1(), input, output),
      );
    });

    group('ascii', () {
      const input = 'Hello World';
      final output = [
        11, 72, 101, 108, 108, //
        111, 32, 87, 111,
        114, 108, 100,
      ];

      test('returns "$input"', expectGet(string.ascii(), output, input));

      test('sets output to "$input"', expectSet(string.ascii(), input, output));
    });
  });

  group('uint8List', () {
    final input = Uint8List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 2];

    test('returns list', expectGet(uint8List(), output, input));

    test('sets list', expectSet(uint8List(), input, output));
  });

  group('uint16List', () {
    final input = Uint16List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 0, 2, 0];

    test('returns list', expectGet(uint16List(), output, input));

    test('sets list', expectSet(uint16List(), input, output));
  });

  group('uint32List', () {
    final input = Uint32List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 0, 0, 0, 2, 0, 0, 0];

    test('returns list', expectGet(uint32List(), output, input));

    test('sets list', expectSet(uint32List(), input, output));
  });

  group('uint64List', () {
    final input = Uint64List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0];

    test('returns list', expectGet(uint64List(), output, input));

    test('sets list', expectSet(uint64List(), input, output));
  });

  group('int8List', () {
    final input = Int8List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 2];

    test('returns list', expectGet(int8List(), output, input));

    test('sets list', expectSet(int8List(), input, output));
  });

  group('int16List', () {
    final input = Int16List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 0, 2, 0];

    test('returns list', expectGet(int16List(), output, input));

    test('sets list', expectSet(int16List(), input, output));
  });

  group('int32List', () {
    final input = Int32List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 0, 0, 0, 2, 0, 0, 0];

    test('returns list', expectGet(int32List(), output, input));

    test('sets list', expectSet(int32List(), input, output));
  });

  group('int64List', () {
    final input = Int64List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0];

    test('returns list', expectGet(int64List(), output, input));

    test('sets list', expectSet(int64List(), input, output));
  });

  group('float32List', () {
    final input = Float32List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 0, 0, 128, 63, 0, 0, 0, 64];

    test('returns list', expectGet(float32List(), output, input));

    test('sets list', expectSet(float32List(), input, output));
  });

  group('float64List', () {
    final input = Float64List(2)
      ..[0] = 1
      ..[1] = 2;
    final output = [2, 0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 0, 64];

    test('returns list', expectGet(float64List(), output, input));

    test('sets list', expectSet(float64List(), input, output));
  });

  group('int32x4List', () {
    final input = Int32x4List(2)
      ..[0] = Int32x4(1, 2, 3, 4)
      ..[1] = Int32x4(1, 2, 3, 4);
    final output = [
      2, //
      1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0,
      1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0,
    ];

    test('returns list', expectGet(int32x4List(), output, input));

    test('sets list', expectSet(int32x4List(), input, output));
  });

  group('float32x4List', () {
    final input = Float32x4List(2)
      ..[0] = Float32x4(1, 2, 3, 4)
      ..[1] = Float32x4(1, 2, 3, 4);
    final output = [
      2, //
      0, 0, 128, 63, 0, 0, 0, 64, 0, 0, 64, 64, 0, 0, 128, 64,
      0, 0, 128, 63, 0, 0, 0, 64, 0, 0, 64, 64, 0, 0, 128, 64,
    ];

    test('returns list', expectGet(float32x4List(), output, input));

    test('sets list', expectSet(float32x4List(), input, output));
  });

  group('float64x2List', () {
    final input = Float64x2List(2)
      ..[0] = Float64x2(1, 2)
      ..[1] = Float64x2(1, 2);
    final output = [
      2, //
      0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 0, 64,
      0, 0, 0, 0, 0, 0, 240, 63, 0, 0, 0, 0, 0, 0, 0, 64,
    ];

    test('returns list', expectGet(float64x2List(), output, input));

    test('sets list', expectSet(float64x2List(), input, output));
  });

  group('byteData', () {
    final input = ByteData(2)
      ..setUint8(0, 1)
      ..setUint8(1, 2);
    final output = [2, 1, 2];

    test('returns list', expectGet(byteData(), output, input));

    test('sets list', expectSet(byteData(), input, output));

    test('sets list', expectSet(byteData(), input, output));
  });

  group('uint8', () {
    test('returns 8', expectGet(uint8, [8], 8));

    test('sets output to 8', expectSet(uint8, 8, [8]));
  });

  group('uint16', () {
    test('returns 16', expectGet(uint16, [0, 16], 16));

    test('sets output to 16', expectSet(uint16, 16, [0, 16]));
  });

  group('uint32', () {
    test('returns 32', expectGet(uint32, [0, 0, 0, 32], 32));

    test('sets output to 32', expectSet(uint32, 32, [0, 0, 0, 32]));
  });

  group('uint64', () {
    test('returns 64', expectGet(uint64, [0, 0, 0, 0, 0, 0, 0, 64], 64));

    test('sets output to 64', expectSet(uint64, 64, [0, 0, 0, 0, 0, 0, 0, 64]));
  });
}

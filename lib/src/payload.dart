part of '../binarize.dart';

/// {@template payload}
/// Base class for the [PayloadWriter] and [PayloadReader].
/// {@endtemplate}
abstract class Payload {
  /// Create a writeable payload.
  ///
  /// An optional [buffer] can be provided, the binary representation of that
  /// buffer will be added to the payload. It wil not write to the buffer but
  /// any changes to the buffer will be reflected in the payload until it is
  /// [binarize]d.
  static PayloadWriter write([ByteBuffer? buffer]) {
    return PayloadWriter._(buffer);
  }

  /// Create a readable payload.
  static PayloadReader read(Iterable<int> data) {
    return PayloadReader._(Uint8List.fromList(data.toList()));
  }

  /// Returns the length of the payload in bytes.
  int get length;
}

/// {@template payload_writer}
/// A writeable payload. It is used by [binarize] to create a binary
/// representation of a payload.
///
/// It can be created by the [Payload.write] method.
/// {@endtemplate}
class PayloadWriter extends Payload {
  PayloadWriter._([ByteBuffer? buffer])
      : _writer = ByteWriter(bytes: buffer?.asUint8List());

  final ByteWriter _writer;

  @override
  int get length => _writer.length;

  /// Set the given [value] using the given [PayloadType].
  void set<T>(PayloadType<T> type, T value) => type.set(_writer, value);
}

/// {@template payload_reader}
/// A readable payload. It is capable of reading values from a binary
/// representation into their corresponding [PayloadType]s.
///
/// It can be created by the [Payload.read] method.
/// {@endtemplate}
class PayloadReader extends Payload {
  PayloadReader._(Uint8List payload)
      : _reader = ByteReader(ByteData.view(payload.buffer));

  final ByteReader _reader;

  @override
  int get length => _reader.length;

  /// Get the next value by using the given [PayloadType].
  T get<T>(PayloadType<T> type) => type.get(_reader);
}

part of '../binarize.dart';

/// Convert a [PayloadWriter] into a [Uint8List] of bytes.
Uint8List binarize(PayloadWriter payload) => payload._writer.toBytes();

import 'package:binarize/binarize.dart';

class _Nil<T> extends PayloadType<T?> {
  const _Nil(this.type);

  final PayloadType<T> type;

  @override
  T? get(ByteReader reader, [Endian? endian]) {
    final isNull = reader.uint8() == 0;
    return isNull ? null : type.get(reader, endian);
  }

  @override
  void set(ByteWriter writer, T? value, [Endian? endian]) {
    writer.uint8(value == null ? 0 : 1);
    if (value != null) type.set(writer, value, endian);
  }
}

/// {@template nil}
/// Make the given [type] accept null values as well.
///
/// When the value is `null`, it will store a single byte with the value `0`.
/// If the value is not `null` it will store a single byte with the value `1`
/// and the encoded result of the [type] right after it.
///
/// It has a byte length of `1 + type.length(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(nil(uint8), null);
/// ```
/// {@endtemplate}
PayloadType<T?> nil<T>(PayloadType<T> type) => _Nil(type);

/// {@macro nil}
@Deprecated('Use nil instead')
PayloadType<T?> nullable<T>(PayloadType<T> type) => _Nil(type);

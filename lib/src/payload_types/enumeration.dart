import 'package:binarize/binarize.dart';

class _Enumeration<T extends Enum> extends PayloadType<T> {
  const _Enumeration(this._values, {PayloadType<int> lengthType = uint8})
      : _lengthType = lengthType;

  final List<T> _values;

  final PayloadType<int> _lengthType;

  @override
  T get(ByteReader reader, [Endian? endian]) =>
      _values.elementAt(_lengthType.get(reader, endian));

  @override
  void set(ByteWriter writer, T value, [Endian? endian]) =>
      _lengthType.set(writer, value.index, endian);
}

/// Store an [Enum] as binary.
PayloadType<T> enumeration<T extends Enum>(
  List<T> values, {
  PayloadType<int> lengthType = uint8,
}) =>
    _Enumeration<T>(values, lengthType: lengthType);

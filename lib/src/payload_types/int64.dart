import 'package:binarize/binarize.dart';

class _Int64 extends PayloadType<int> {
  const _Int64();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.int64(endian);

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.int64(value, endian);
}

/// Converts a integer into eight bytes that represents an Int64 value.
///
/// The value has to be between -2⁶³ and 2⁶³ - 1, inclusive.
///
/// It has a byte length of 8.
///
/// Usage:
///
/// ```dart
/// payload.set(int64, 250);
/// ```
const int64 = _Int64();

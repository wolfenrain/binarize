import 'package:binarize/binarize.dart';

class _Int8 extends PayloadType<int> {
  const _Int8();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.int8();

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.int8(value);
}

/// Converts an integer into a single byte that represents an Int8 value.
///
/// The value has to be between -128 and 127, inclusive.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(int8, 100);
/// ```
const int8 = _Int8();

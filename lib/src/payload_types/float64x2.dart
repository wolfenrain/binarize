import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Float64x2 extends PayloadType<Float64x2> {
  const _Float64x2();

  @override
  Float64x2 get(ByteReader reader, [Endian? endian]) {
    return Float64x2(reader.float64(), reader.float64());
  }

  @override
  void set(ByteWriter writer, Float64x2 value, [Endian? endian]) {
    writer
      ..float64(value.x)
      ..float64(value.y);
  }
}

/// Converts a [Float64x2] into two float32 sets that represents a
/// Float64x2 value.
///
/// Usage:
///
/// ```dart
/// payload.set(float64x2, Float64x2.zero());
/// ```
const float64x2 = _Float64x2();

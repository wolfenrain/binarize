import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _TypedData<T extends TypedData> extends PayloadType<T> {
  const _TypedData(this._lengthType);

  final PayloadType<int> _lengthType;

  @override
  T get(ByteReader reader, [Endian? endian]) {
    final lengthInElements = _lengthType.get(reader);

    if (T == Uint8List) {
      return Uint8List.fromList(
        reader.read(lengthInElements * Uint8List.bytesPerElement),
      ) as T;
    } else if (T == Uint16List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Uint16List.bytesPerElement),
      ).buffer;
      return buffer.asUint16List() as T;
    } else if (T == Uint32List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Uint32List.bytesPerElement),
      ).buffer;
      return buffer.asUint32List() as T;
    } else if (T == Uint64List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Uint64List.bytesPerElement),
      ).buffer;
      return buffer.asUint64List() as T;
    } else if (T == Int8List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Int8List.bytesPerElement),
      ).buffer;
      return buffer.asInt8List() as T;
    } else if (T == Int16List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Int16List.bytesPerElement),
      ).buffer;
      return buffer.asInt16List() as T;
    } else if (T == Int32List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Int32List.bytesPerElement),
      ).buffer;
      return buffer.asInt32List() as T;
    } else if (T == Int64List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Int64List.bytesPerElement),
      ).buffer;
      return buffer.asInt64List() as T;
    } else if (T == Float32List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Float32List.bytesPerElement),
      ).buffer;
      return buffer.asFloat32List() as T;
    } else if (T == Float64List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Float64List.bytesPerElement),
      ).buffer;
      return buffer.asFloat64List() as T;
    } else if (T == Int32x4List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Int32x4List.bytesPerElement),
      ).buffer;
      return buffer.asInt32x4List() as T;
    } else if (T == Float32x4List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Float32x4List.bytesPerElement),
      ).buffer;
      return buffer.asFloat32x4List() as T;
    } else if (T == Float64x2List) {
      final buffer = Uint8List.fromList(
        reader.read(lengthInElements * Float64x2List.bytesPerElement),
      ).buffer;
      return buffer.asFloat64x2List() as T;
    } else if (T == ByteData) {
      final buffer = Uint8List.fromList(reader.read(lengthInElements)).buffer;
      return buffer.asByteData() as T;
    }

    // coverage:ignore-start unreachable code
    throw UnimplementedError('$T');
    // coverage:ignore-end
  }

  @override
  void set(ByteWriter writer, T value, [Endian? endian]) {
    _lengthType.set(writer, value.lengthInBytes ~/ value.elementSizeInBytes);
    return writer.write(value);
  }
}

/// Converts a list of unsigned integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(uint8List(), Uint8List.fromList([...]));
/// ```
PayloadType<Uint8List> uint8List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of unsigned integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(uint16List(), Uint16List.fromList([...]));
/// ```
PayloadType<Uint16List> uint16List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of unsigned integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(uint32List(), Uint32List.fromList([...]));
/// ```
PayloadType<Uint32List> uint32List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of unsigned integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(uint64List(), Uint64List.fromList([...]));
/// ```
PayloadType<Uint64List> uint64List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of signed integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(int8List(), Int8List.fromList([...]));
/// ```
PayloadType<Int8List> int8List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of signed integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(int16List(), Int16List.fromList([...]));
/// ```
PayloadType<Int16List> int16List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of signed integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(int32List(), Int32List.fromList([...]));
/// ```
PayloadType<Int32List> int32List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of signed integers into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(int64List(), Int64List.fromList([...]));
/// ```
PayloadType<Int64List> int64List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of floats into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(float32List(), Float32List.fromList([...]));
/// ```
PayloadType<Float32List> float32List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of floats into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(float64List(), Float64List.fromList([...]));
/// ```
PayloadType<Float64List> float64List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of [Int32x4] into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(int32x4List(), Int32x4List.fromList([...]));
/// ```
PayloadType<Int32x4List> int32x4List({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

/// Converts a list of [Float32x4] into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(float32x4List(), Float32x4List.fromList([...]));
/// ```
PayloadType<Float32x4List> float32x4List({
  PayloadType<int> lengthType = uint8,
}) =>
    _TypedData(lengthType);

/// Converts a list of [Float64x2] into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(float64x2List(), Float64x2List.fromList([...]));
/// ```
PayloadType<Float64x2List> float64x2List({
  PayloadType<int> lengthType = uint8,
}) =>
    _TypedData(lengthType);

/// Converts byte data into binary.
///
/// Usage:
///
/// ```dart
/// payload.set(byteData(), ByteData.view([...]));
/// ```
PayloadType<ByteData> byteData({PayloadType<int> lengthType = uint8}) =>
    _TypedData(lengthType);

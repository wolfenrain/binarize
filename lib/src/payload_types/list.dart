import 'package:binarize/binarize.dart';

class _List<T> extends PayloadType<List<T>> {
  const _List(this.type, this.lengthType);

  final PayloadType<T> type;

  final PayloadType<int> lengthType;

  @override
  List<T> get(ByteReader reader, [Endian? endian]) {
    final length = lengthType.get(reader, endian);
    return [for (var i = 0; i < length; i++) type.get(reader, endian)];
  }

  @override
  void set(ByteWriter writer, List<T> value, [Endian? endian]) {
    lengthType.set(writer, value.length, endian);
    for (var i = 0; i < value.length; i++) {
      type.set(writer, value[i], endian);
    }
  }
}

/// Make the given [type] support a list of [T].
///
/// It stores the length of the list first by using the [lengthType] and then
/// store each element of the list using the [type].
///
/// It has a byte length of `lengthType.length(list.first) + type.length(list)`.
///
/// Usage:
///
/// ```dart
/// payload.set(list(uint8), [1, 2 ,3, 4]);
/// ```
PayloadType<List<T>> list<T>(
  PayloadType<T> type, {
  PayloadType<int> lengthType = uint32,
}) =>
    _List(type, lengthType);

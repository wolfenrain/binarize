import 'package:binarize/binarize.dart';

/// {@template bytes}
/// Reads and write a list of bytes.
///
/// The integers in the list have to be [uint8].
///
/// It has a byte length of `list.length`.
///
/// Usage:
///
/// ```dart
/// payload.set(Bytes(3), [1, 2, 3]);
/// ```
/// {@endtemplate}
class Bytes extends RawList<int> {
  /// {@macro bytes}
  const Bytes(int byteLength) : super(uint8, amount: byteLength);
}

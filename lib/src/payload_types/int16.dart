import 'package:binarize/binarize.dart';

class _Int16 extends PayloadType<int> {
  const _Int16();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.int16(endian);

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.int16(value, endian);
}

/// Converts a integer into two bytes that represents an Int16 value.
///
/// The value has to be between -2¹⁵ and 2¹⁵ - 1, inclusive.
///
/// It has a byte length of 2.
///
/// Usage:
///
/// ```dart
/// payload.set(int16, 10000);
/// ```
const int16 = _Int16();

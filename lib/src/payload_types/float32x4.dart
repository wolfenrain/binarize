import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Float32x4 extends PayloadType<Float32x4> {
  const _Float32x4();

  @override
  Float32x4 get(ByteReader reader, [Endian? endian]) {
    return Float32x4(
      reader.float32(),
      reader.float32(),
      reader.float32(),
      reader.float32(),
    );
  }

  @override
  void set(ByteWriter writer, Float32x4 value, [Endian? endian]) {
    writer
      ..float32(value.x)
      ..float32(value.y)
      ..float32(value.z)
      ..float32(value.w);
  }
}

/// Converts a [Float32x4] into four float32 sets that represents a
/// Float32x4 value.
///
/// Usage:
///
/// ```dart
/// payload.set(float32x4, Float32x4.zero());
/// ```
const float32x4 = _Float32x4();

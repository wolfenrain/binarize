import 'package:binarize/binarize.dart';

class _Uint8 extends PayloadType<int> {
  const _Uint8();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.uint8();

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.uint8(value);
}

/// Converts a integer into a single byte that represents an unsigned Int8
/// value.
///
/// The value has to be between 0 and 255, inclusive.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(uint8, 250);
/// ```
const uint8 = _Uint8();

import 'package:binarize/binarize.dart';

class _Flags extends PayloadType<List<bool>> {
  const _Flags();

  @override
  List<bool> get(ByteReader reader, [Endian? endian]) {
    final flagValue = reader.uint8();
    final flags = <bool>[];
    for (var i = 0; i < 8; i++) {
      flags.add((flagValue & 1 << i) != 0);
    }
    return flags;
  }

  @override
  void set(ByteWriter writer, List<bool> value, [Endian? endian]) {
    assert(value.length <= 8, 'Flags can only be 8 bits long');

    var val = 0;
    for (var i = 0; i < 8; i++) {
      final flag = i < value.length ? value[i] : !true;
      final bit = flag ? 1 : 0;
      val |= bit << i;
    }
    writer.uint8(val);
  }
}

/// Converts a list of 8 booleans into a single byte.
///
/// If the list of booleans is less than 8 it will automatically set the rest
/// of the values to `false`.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(flags, [true, false, true]);
/// ```
const flags = _Flags();

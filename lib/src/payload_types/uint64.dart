import 'package:binarize/binarize.dart';

class _Uint64 extends PayloadType<int> {
  const _Uint64();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.uint64(endian);

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.uint64(value, endian);
}

/// Converts a integer into eight bytes that represents an unsigned Int64 value.
///
/// The value has to be between 0 and 2⁶⁴ - 1, inclusive.
///
/// It has a byte length of 8.
///
/// Usage:
///
/// ```dart
/// payload.set(uint64, 250);
/// ```
const uint64 = _Uint64();

import 'dart:convert' as encoding show ascii, latin1, utf8;
import 'dart:convert' show Encoding;

import 'package:binarize/binarize.dart';

class _StringEncoding {
  const _StringEncoding();

  /// Converts a string into an encoded UTF-8 list.
  ///
  /// The length will be stored using the [lengthType] and therefore it has a
  /// byte length of `lengthType(value) + len(value)`.
  ///
  /// Usage:
  ///
  /// ```dart
  /// payload.set(string.utf8(), 'Hello world');
  /// ```
  PayloadType<String> utf8({PayloadType<int> lengthType = uint8}) =>
      call(encoding.utf8, lengthType: lengthType);

  /// Converts a string into an encoded ISO/IEC 8859-1 (also known as Latin 1)
  /// list.
  ///
  /// The length will be stored using the [lengthType] and therefore it has a
  /// byte length of `lengthType(value) + len(value)`.
  ///
  /// Usage:
  ///
  /// ```dart
  /// payload.set(string.latin1(), 'Hello world');
  /// ```
  PayloadType<String> latin1({PayloadType<int> lengthType = uint8}) =>
      call(encoding.latin1, lengthType: lengthType);

  /// Converts a string into an encoded ASCII list.
  ///
  /// The length will be stored using the [lengthType] and therefore it has a
  /// byte length of `lengthType(value) + len(value)`.
  ///
  /// Usage:
  ///
  /// ```dart
  /// payload.set(string.ascii(), 'Hello world');
  /// ```
  PayloadType<String> ascii({PayloadType<int> lengthType = uint8}) =>
      call(encoding.ascii, lengthType: lengthType);

  /// Converts a string into an encoded list by using the provided [encoding].
  ///
  /// The length will be stored using the [lengthType] and therefore it has a
  /// byte length of `lengthType(value) + len(value)`.
  ///
  /// Usage:
  ///
  /// ```dart
  /// payload.set(string(MyOwnEncoding()), 'Hello world');
  /// ```
  PayloadType<String> call(
    Encoding encoding, {
    PayloadType<int> lengthType = uint8,
  }) {
    return _String(lengthType, encoding);
  }
}

class _String extends PayloadType<String> {
  const _String(this._lengthType, this._encoding);

  final PayloadType<int> _lengthType;

  final Encoding _encoding;

  @override
  String get(ByteReader reader, [Endian? endian]) {
    final length = _lengthType.get(reader, endian);
    return _encoding.decode(reader.read(length));
  }

  @override
  void set(ByteWriter writer, String value, [Endian? endian]) {
    final encoded = _encoding.encode(value);
    _lengthType.set(writer, encoded.length, endian);
    writer.write(encoded is Uint8List ? encoded : Uint8List.fromList(encoded));
  }
}

/// Provide a subset of string encoders.
///
/// The following default Dart encoders are supported:
/// - `string.utf8`
/// - `string.latin1`
/// - `string.ascii`
///
/// But you can also supply your own [Encoding] by calling this object directly:
///
/// ```dart
/// const myEncodingType = string(MyEncoding());
/// ```
const string = _StringEncoding();

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint8. Therefore it has a byte length of
/// `1 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string8, 'Hello world');
/// ```
@Deprecated('Use string.utf8(lengthType: uint8) instead')
final string8 = string.utf8();

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint16. Therefore it has a byte length of
/// `2 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string16, 'Hello world');
/// ```
@Deprecated('Use string.utf8(lengthType: uint16) instead')
final string16 = string.utf8(lengthType: uint16);

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint8. Therefore it has a byte length of
/// `4 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string32, 'Hello world');
/// ```
@Deprecated('Use string.utf8(lengthType: uint32) instead')
final string32 = string.utf8(lengthType: uint32);

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint64. Therefore it has a byte length of
/// `8 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string64, 'Hello world');
/// ```
@Deprecated('Use string.utf8(lengthType: uint64) instead')
final string64 = string.utf8(lengthType: uint64);

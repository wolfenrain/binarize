import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Int32x4 extends PayloadType<Int32x4> {
  const _Int32x4();

  @override
  Int32x4 get(ByteReader reader, [Endian? endian]) {
    return Int32x4(
      reader.int32(),
      reader.int32(),
      reader.int32(),
      reader.int32(),
    );
  }

  @override
  void set(ByteWriter writer, Int32x4 value, [Endian? endian]) {
    writer
      ..int32(value.x)
      ..int32(value.y)
      ..int32(value.z)
      ..int32(value.w);
  }
}

/// Converts a [Int32x4] into four int32 sets that represents an Int32x4 value.
///
/// Usage:
///
/// ```dart
/// payload.set(int32x4, Int32x4.bool(true, false, true, true));
/// ```
const int32x4 = _Int32x4();

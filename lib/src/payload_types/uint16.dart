import 'package:binarize/binarize.dart';

class _Uint16 extends PayloadType<int> {
  const _Uint16();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.uint16(endian);

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.uint16(value, endian);
}

/// Converts a integer into two bytes that represents an unsigned Int16 value.
///
/// The value has to be between 0 and 2¹⁶ - 1, inclusive.
///
/// It has a byte length of 2.
///
/// Usage:
///
/// ```dart
/// payload.set(uint16, 250);
/// ```
const uint16 = _Uint16();

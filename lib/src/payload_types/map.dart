import 'package:binarize/binarize.dart';

class _Map<K, V> extends PayloadType<Map<K, V>> {
  const _Map(this.keyType, this.valueType, this.lengthType);

  final PayloadType<K> keyType;

  final PayloadType<V> valueType;

  final PayloadType<int> lengthType;

  @override
  Map<K, V> get(ByteReader reader, [Endian? endian]) {
    final length = lengthType.get(reader);

    return Map.fromEntries([
      for (var i = 0; i < length; i++)
        MapEntry(keyType.get(reader, endian), valueType.get(reader, endian)),
    ]);
  }

  @override
  void set(ByteWriter writer, Map<K, V> value, [Endian? endian]) {
    lengthType.set(writer, value.length);

    for (final k in value.keys) {
      keyType.set(writer, k);
      valueType.set(writer, value[k] as V, endian);
    }
  }
}

/// Store a map of [K] and [V] as binary data.
///
/// It stores the length of the map first by using the [lengthType] and then
/// stores each key value pair in the map by looping through the keys, storing
/// the key first using [keyType] and then the value of the key using
/// [valueType].
///
/// It has a byte length of:
/// ```dart
/// lengthType.length(map.length) + map.entries.fold(0,
///   (p, e) => p + keyType.length(e.key) + valueType.length(e.value),
/// )
/// ```
///
/// Usage:
///
/// ```dart
/// payload.set(map(string8, uint16), {'1': 1, '2': 2, '3': 3, '4': 4});
/// ```
PayloadType<Map<K, V>> map<K, V>(
  PayloadType<K> keyType,
  PayloadType<V> valueType, {
  PayloadType<int> lengthType = uint8,
}) {
  return _Map(keyType, valueType, lengthType);
}

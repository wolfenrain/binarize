import 'package:binarize/binarize.dart';

/// {@template raw_list}
/// Reads and write a raw list of [type].
///
/// It has a byte length of `list.fold(0, (p, e) => p + type.length(e))`.
///
/// Usage:
///
/// ```dart
/// payload.set(RawList(string8, amount: 3), ['1', '2', '3']);
/// ```
/// {@endtemplate}
class RawList<T> extends PayloadType<List<T>> {
  /// {@macro raw_list}
  const RawList(this.type, {required this.amount});

  /// The amount of values this type supports.
  final int amount;

  /// The payload type to use.
  final PayloadType<T> type;

  @override
  List<T> get(ByteReader reader, [Endian? endian]) {
    return <T>[for (var i = 0; i < amount; i++) type.get(reader, endian)];
  }

  @override
  void set(ByteWriter writer, List<T> value, [Endian? endian]) {
    for (var i = 0; i < amount; i++) {
      type.set(writer, value[i], endian);
    }
  }
}

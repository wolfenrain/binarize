import 'dart:async';

import 'package:binarize/binarize.dart';

/// {@template binary_contract}
/// The [BinaryContract] allows developers to define a strongly typed binary
/// representation of a Dart class. If your Dart class contract changes, so will
/// the binary contract, ensuring you never forget to change how the data is
/// being read or stored.
///
/// [BinaryContract] works best with classes that are immutable, because it will
/// implement your class by overwriting the fields as getters. If your fields
/// are not `final` Dart will complain that the setter was not implement. There
/// are ways to work around this but it is not recommended.
///
///
/// An example of how you can implement a contract for your own classes:
///
/// ```dart
/// class MyClass {
///   const MyClass({
///     required this.myProperty,
///   });
///
///   final int myProperty;
/// }
///
/// /// Defining the contract for the [MyClass] class. It should also implement the
/// /// class so the contract can access the fields.
/// class _MyClassContract extends BinaryContract<MyClass> implements MyClass {
///   /// Pass a base line instance to the super constructor. This is used to build
///   /// up the, contract, the values defined in the base line do not matter.
///   const _MyClassContract()
///       : super(const MyClass(myProperty: 1337));
///
///   /// This method allows the [BinaryContract] to know the order of how the binary
///   /// data should be read and stored.
///   ///
///   /// [contract] is always the [BinaryContract] instance.
///   @override
///   MyClass order(MyClass contract) {
///     return MyClass(
///       myProperty: contract.myProperty,
///     );
///   }
///
///   /// The properties of the [MyClass] need to be implemented as getters.
///   @override
///   int get myProperty => type(uint16, (o) => o.myProperty);
/// }
///
/// // Create a constant instance of the contract.
/// const myClassContract= _MyClassContract();
///
/// void main() {
///   // Create a new instance of MyClass.
///   final myClass = MyClass(myProperty: 1337);
///
///   // Store the instance using the contract.
///   final writer = Payload.write()..set(myClassContract, myClass);
///   final bytes = binarize(writer);
///
///   // Read it back from binary.
///   final reader = Payload.read(bytes);
///   final myClassAgain = reader.get(myClassContract);
///
///   print(myClassAgain.myProperty); // Returns 1337
/// }
/// ```
///
/// The [BinaryContract] extends from [PayloadType] and can be used within other
/// payloads, including itself.
/// {@endtemplate}
abstract class BinaryContract<T> extends PayloadType<T> {
  /// {@macro binary_contract}
  const BinaryContract(this._base);

  final T _base;

  static final Map<Type, List<_BinaryField<Object, Object?>>> _allFields = {};

  List<_BinaryField<T, Object?>> get _fields {
    // If the fields have not yet been retrieved, retrieve them by calling the
    // order method, which in turn will call the user-defined type calls.
    if (!_allFields.containsKey(T)) {
      final fields = <_BinaryField<T, Object?>>[];
      _allFields[T] = fields as List<_BinaryField<Object, Object?>>;

      // Running it in a special zone so that the type method can directly add
      // the binary fields to the list without having to look up the list.
      runZoned(() => order(this as T), zoneValues: {#fields: fields});
    }
    return _allFields[T]! as List<_BinaryField<T, Object?>>;
  }

  /// Define what the order is in which the binary data should be read or
  /// stored.
  ///
  /// [contract] is always the [BinaryContract] instance.
  T order(T contract);

  /// Define a property's type and how the value should be resolved on the
  /// object type ([T]).
  V type<V>(PayloadType<V> type, V Function(T o) resolve) {
    (Zone.current[#fields] as List<_BinaryField>?)
        ?.add(_BinaryField<T, V>(type, resolve));
    if (Zone.current[#values] != null) {
      return (Zone.current[#values] as List<Object?>).removeAt(0) as V;
    }
    return resolve(_base);
  }

  @override
  T get(ByteReader reader, [Endian? endian]) {
    final values = <Object?>[];
    for (final field in _fields) {
      field.get(reader, values, endian);
    }
    return runZoned<T>(() => order(this as T), zoneValues: {#values: values});
  }

  @override
  void set(ByteWriter writer, T value, [Endian? endian]) {
    for (final field in _fields) {
      field.set(writer, value);
    }
  }
}

class _BinaryField<T, V> {
  const _BinaryField(this.type, this.resolve);

  /// The type used for this field.
  final PayloadType<V> type;

  /// Resolves the value out of the object.
  final V Function(T) resolve;

  void get(ByteReader reader, List<Object?> values, [Endian? endian]) =>
      values.add(type.get(reader, endian));

  void set(ByteWriter writer, T object, [Endian? endian]) =>
      type.set(writer, resolve(object), endian);
}

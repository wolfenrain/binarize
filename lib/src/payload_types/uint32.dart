import 'package:binarize/binarize.dart';

class _Uint32 extends PayloadType<int> {
  const _Uint32();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.uint32(endian);

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.uint32(value, endian);
}

/// Converts a integer into four bytes that represents an unsigned Int32 value.
///
/// The value has to be between 0 and 2³² - 1, inclusive.
///
/// It has a byte length of 4.
///
/// Usage:
///
/// ```dart
/// payload.set(uint32, 250);
/// ```
const uint32 = _Uint32();

import 'package:binarize/binarize.dart';

class _Int32 extends PayloadType<int> {
  const _Int32();

  @override
  int get(ByteReader reader, [Endian? endian]) => reader.int32(endian);

  @override
  void set(ByteWriter writer, int value, [Endian? endian]) =>
      writer.int32(value, endian);
}

/// Converts a integer into four bytes that represents an Int32 value.
///
/// The value has to be between -2³¹ and 2³¹ - 1, inclusive.
///
/// It has a byte length of 4.
///
/// Usage:
///
/// ```dart
/// payload.set(int32, 1000000);
/// ```
const int32 = _Int32();

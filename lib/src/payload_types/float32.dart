import 'package:binarize/binarize.dart';

class _Float32 extends PayloadType<double> {
  const _Float32();

  @override
  double get(ByteReader reader, [Endian? endian]) => reader.float32(endian);

  @override
  void set(ByteWriter writer, double value, [Endian? endian]) =>
      writer.float32(value, endian);
}

/// Converts a double into four bytes that represents a Float32 value.
///
/// It has a byte length of 4.
///
/// **Note**: this payload type can lose precision. The input value is a 64-bit
/// floating point value, which will be converted to 32-bit floating point
/// value by IEEE 754 rounding rules before it is stored. If the value cannot
/// be represented exactly as a binary32, it will be converted to the nearest
/// binary32 value. If two binary32 values are equally close, the one whose
/// least significant bit is zero will be used.
///
/// **Note**: finite (but large) values can be converted to
/// infinity, and small non-zero values can be converted to zero.
///
/// Usage:
///
/// ```dart
/// payload.set(float32, 1.5);
/// ```
const float32 = _Float32();

import 'dart:typed_data';

import 'package:binarize/src/byte_reader.dart';
import 'package:binarize/src/byte_writer.dart';

/// {@template payload_type}
/// The base class for any payload type. It is capable of reading and writing
/// values to a `PayloadWriter` or `PayloadReader`.
///
/// Each [PayloadType] handles it's own packing and unpacking of bytes.
/// {@endtemplate}
abstract class PayloadType<T> {
  /// {@macro payload_type}
  const PayloadType();

  /// Retrieve the value from the [reader].
  T get(ByteReader reader, [Endian? endian]);

  /// Set the [value] in the [writer].
  void set(ByteWriter writer, T value, [Endian? endian]);
}

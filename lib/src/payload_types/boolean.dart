import 'package:binarize/binarize.dart';

class _Boolean extends PayloadType<bool> {
  const _Boolean();

  @override
  bool get(ByteReader reader, [Endian? endian]) => reader.uint8() == 1;

  @override
  void set(ByteWriter writer, bool value, [Endian? endian]) =>
      writer.uint8(value ? 1 : 0);
}

/// Converts a boolean into a single bit.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(boolean, [true, false, true]);
/// ```
const boolean = _Boolean();

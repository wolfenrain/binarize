import 'package:binarize/binarize.dart';

class _Float64 extends PayloadType<double> {
  const _Float64();

  @override
  double get(ByteReader reader, [Endian? endian]) => reader.float64(endian);

  @override
  void set(ByteWriter writer, double value, [Endian? endian]) =>
      writer.float64(value, endian);
}

/// Converts a double into eight bytes that represents a Float64 value.
///
/// It has a byte size of 8.
///
/// Usage:
///
/// ```dart
/// payload.set(float64, 1.5);
/// ```
const float64 = _Float64();

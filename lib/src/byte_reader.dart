import 'dart:typed_data';

/// {@template byte_reader}
/// Provides a stream lined experience for reading binary data.
/// {@endtemplate}
class ByteReader {
  /// {@macro byte_reader}
  ByteReader(TypedData data, {Endian endian = Endian.big})
      : _data = data.buffer.asByteData(),
        _endian = endian;

  final ByteData _data;

  final Endian _endian;

  /// The buffer of the data that is currently being read.
  ByteBuffer get buffer => _data.buffer;

  /// Returns the length of bytes in the reader.
  int get length => _data.lengthInBytes;

  /// Returns the current position in the byte data.
  int get offset => _offset;
  int _offset = 0;

  /// The remaining bytes left to read.
  int get remainingBytes => _data.lengthInBytes - _offset;

  /// Returns `true` if done reading.
  bool get isDone => _offset >= _data.lengthInBytes;

  /// Returns `true` if not yet done reading.
  bool get isNotDone => !isDone;

  /// Read an uint8 value.
  int uint8() => _data.getUint8(_progress(1));

  /// Read an uint16 value.
  int uint16([Endian? endian]) =>
      _data.getUint16(_progress(2), endian ?? _endian);

  /// Read an uint32 value.
  int uint32([Endian? endian]) =>
      _data.getUint32(_progress(4), endian ?? _endian);

  /// Read an uint64 value.
  int uint64([Endian? endian]) =>
      _data.getUint64(_progress(8), endian ?? _endian);

  /// Read an int8 value.
  int int8() => _data.getInt8(_progress(1));

  /// Read an int16 value.
  int int16([Endian? endian]) =>
      _data.getInt16(_progress(2), endian ?? _endian);

  /// Read an int32 value.
  int int32([Endian? endian]) =>
      _data.getInt32(_progress(4), endian ?? _endian);

  /// Read an int64 value.
  int int64([Endian? endian]) =>
      _data.getInt64(_progress(8), endian ?? _endian);

  /// Read a float32 value.
  double float32([Endian? endian]) =>
      _data.getFloat32(_progress(4), endian ?? _endian);

  /// Read a float64 value.
  double float64([Endian? endian]) =>
      _data.getFloat64(_progress(8), endian ?? _endian);

  /// Read bytes of a given [length], or all of the [remainingBytes].
  Uint8List read([int? length]) {
    final offset = _progress(length ?? remainingBytes);
    return Uint8List.sublistView(_data, offset, _offset);
  }

  /// Reset the reader.
  void reset() => _offset = 0;

  /// Skip the amount of [lengthInBytes].
  void skip(int lengthInBytes) => _progress(lengthInBytes);

  int _progress(int lengthInBytes) {
    final offset = _offset;
    _offset += lengthInBytes;
    return offset;
  }
}

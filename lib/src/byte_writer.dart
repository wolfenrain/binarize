import 'dart:typed_data';

/// {@template byte_writer}
/// Provides a stream lined experience for writing binary data.
/// {@endtemplate}
class ByteWriter {
  /// {@macro byte_writer}
  ByteWriter({
    Endian endian = Endian.big,
    Uint8List? bytes,
  })  : _builder = BytesBuilder()..add(bytes ?? []),
        _endian = endian;

  final BytesBuilder _builder;

  final Endian _endian;

  /// Returns the length of bytes in the writer.
  int get length => _builder.length;

  /// Write an uint8 [value].
  void uint8(int value) => write(ByteData(1)..setUint8(0, value));

  /// Write an uint16 [value].
  void uint16(int value, [Endian? endian]) =>
      write(ByteData(2)..setUint16(0, value, endian ?? _endian));

  /// Write an uint32 [value].
  void uint32(int value, [Endian? endian]) =>
      write(ByteData(4)..setUint32(0, value, endian ?? _endian));

  /// Write an uint64 [value].
  void uint64(int value, [Endian? endian]) =>
      write(ByteData(8)..setUint64(0, value, endian ?? _endian));

  /// Write an int8 [value].
  void int8(int value) => write(ByteData(1)..setInt8(0, value));

  /// Write an int16 [value].
  void int16(int value, [Endian? endian]) =>
      write(ByteData(2)..setInt16(0, value, endian ?? _endian));

  /// Write an int32 [value].
  void int32(int value, [Endian? endian]) =>
      write(ByteData(4)..setInt32(0, value, endian ?? _endian));

  /// Write an int64 [value].
  void int64(int value, [Endian? endian]) =>
      write(ByteData(8)..setInt64(0, value, endian ?? _endian));

  /// Write a float32 [value].
  void float32(double value, [Endian? endian]) =>
      write(ByteData(4)..setFloat32(0, value, endian ?? _endian));

  /// Write a float64 [value].
  void float64(double value, [Endian? endian]) =>
      write(ByteData(8)..setFloat64(0, value, endian ?? _endian));

  /// Write [data].
  void write(TypedData data) => _builder.add(data.buffer.asUint8List());

  /// Get the written bytes.
  Uint8List toBytes() => _builder.toBytes();

  /// Clear any written bytes.
  void clear() => _builder.clear();
}

import 'package:binarize/binarize.dart';

class MyClass {
  const MyClass({required this.myProperty});

  final int myProperty;
}

class _MyClassContract extends BinaryContract<MyClass> implements MyClass {
  const _MyClassContract() : super(const MyClass(myProperty: 1337));

  @override
  MyClass order(MyClass format) => MyClass(myProperty: format.myProperty);

  @override
  int get myProperty => type(uint16, (o) => o.myProperty);
}

const myClassContract = _MyClassContract();

void main() {
  const myClass = MyClass(myProperty: 1337);

  // Write value types to the PayloadWriter.
  final writer = Payload.write()
    ..set(boolean, true) // Store a boolean.
    ..set(flags, [true, false, true]) // Store a list of booleans.

    ..set(float32, 1.5) // Store a double as a float32.
    ..set(float64, 3.5) // Store a double as a float64.

    ..set(int8, 5) // Store an int as int8.
    ..set(int16, -15) // Store an int as int16.
    ..set(int32, 38) // Store an int as int32.
    ..set(int64, -2050) // Store an int as int64.

    ..set(string.utf8(), 'Hello world') // Store a string with a fixed length.

    ..set(uint8, 5) // Store an int as uint8.
    ..set(uint16, 15) // Store an int as uint16.
    ..set(uint32, 30) // Store an int as uint32.
    ..set(uint64, 60) // Store an int as uint64.

    ..set(const Bytes(3), [1, 2, 3]) // Store a list of 3 bytes.
    ..set(nil(boolean), null) // Store a null boolean value.
    ..set(list(uint8), [1, 2, 3, 4, 5]) // Store a list of uint8 values.
    ..set(map(string.utf8(), uint8), {'1': 1, '2': 2, '3': 3}) // Store a map.
    ..set(RawList(string.utf8(), amount: 2), ['1', '2']) // Store a raw list
    ..set(myClassContract, myClass); // Store a class by it's own contract.

  // Converting the payload to a list of bytes.
  final byteList = binarize(writer);

  // Read the bytes back into values using PayloadReader.
  Payload.read(byteList)
    ..get(boolean) // Get a boolean value.
    ..get(flags) // Get a list of 8 booleans, representing flags.

    ..get(float32) // Get a float32 value as a double.
    ..get(float64) // Get a float64 value as a double.

    ..get(int8) // Get an int8 value as an int.
    ..get(int16) // Get an int16 value as an int.
    ..get(int32) // Get an int32 value as an int.
    ..get(int64) // Get an int64 value as an int.

    ..get(string.utf8()) // Get a string value.

    ..get(uint8) // Get an uint8 value as an int.
    ..get(uint16) // Get an uint16 value as an int.
    ..get(uint32) // Get an uint32 value as an int.
    ..get(uint64) // Get an uint64 value as an int.

    ..get(const Bytes(3)) // Get a list of 3 bytes.
    ..get(nil(boolean)) // Get a null boolean value.
    ..get(list(uint8)) // Get a list of uint8 values.
    ..get(map(string.utf8(), uint8)) // Get a map of key(string) => value(int)
    ..get(RawList(string.utf8(), amount: 2)) // Get a raw list of strings.
    ..get(myClassContract); // Get a class using it's own contract.
}

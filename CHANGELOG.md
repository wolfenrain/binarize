## 2.0.0

- feat!: reworked the API to be more efficient in reading and writing binary data.
- feat!: string encoding now supports the Dart `Encoding` type for encoding.
  - **Deprecated**: The `string8`, `string16`, `string32` and `string64` are deprecated in favor of the new `string` encoding.
- chore!: removed deprecated `string` constant.
- chore!: updated SDK lower constraint to `2.15.0`.
- fix: renamed `nullable` to `nil` to reflect the same naming convention used for other types.
  - **Deprecated**: The `nullable` type is deprecated in favor of the new `nil` type. 
- feat: add `enumeration` for storing `Enum` values.
- feat: add different `TypedData` methods for storing binary buffers more efficiently.
- feat: add `int32x4`, `float32x4` and `float64x2` for storing SIMD instructions.
- chore: updated `very_good_analysis` to `7.0.0`.
- chore: updated `test` to `1.25.14`.

## 1.5.0

- feat: add `BinaryContract` for defining a strongly typed binary contract for Dart classes.

## 1.4.0

- feat: added `map` payload type for storing pairs of keys and values as a map.
- feat: added `RawList` payload type for reading and writing lists with a constant length.
- refactor: `Bytes` now uses the new `RawList` payload type.
- feat: exporting `Uint8List` and `ByteData` from `dart:typed_data`.

## 1.3.1

- fix: calculated length of `list` was off.

## 1.3.0

- feat: added `list` payload type for storing lists of values of any other payload type.

## 1.2.0

- feat: added `nullable` payload type for storing nullable values of any other payload type.
- docs: added a small section to the README about a use case.

## 1.1.0

- feat: added `Bytes` payload type for reading and writing list of bytes.
- feat: added support for reading and writing strings with lower and higher length storage.
  - **Deprecated**: The `string` payload type is now deprecated in favor of the `string32` payload type.
- docs: improved documentation for most types.
- feat: `PayloadReader` now has a `offset` property that indicates how far it has read.

## 1.0.1

- Improved documentation, tests and internal code.

## 1.0.0+1

- Exposing the writer and reader classes.

## 1.0.0

- Initial release of Binarize.
